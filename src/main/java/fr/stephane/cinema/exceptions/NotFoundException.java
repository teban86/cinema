package fr.stephane.cinema.exceptions;

public class NotFoundException extends RuntimeException {

	public NotFoundException(String message) {
		super(message);
	
	}

}
