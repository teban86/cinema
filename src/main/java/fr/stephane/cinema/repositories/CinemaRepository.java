package fr.stephane.cinema.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.stephane.cinema.models.Cinema;

public interface CinemaRepository extends MongoRepository<Cinema, String> {

}
