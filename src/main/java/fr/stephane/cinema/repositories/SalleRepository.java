package fr.stephane.cinema.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.stephane.cinema.models.Salle;

public interface SalleRepository extends MongoRepository<Salle, String> {

}
