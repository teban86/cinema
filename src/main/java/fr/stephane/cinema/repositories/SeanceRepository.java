package fr.stephane.cinema.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.stephane.cinema.models.Seance;

public interface SeanceRepository extends MongoRepository<Seance, String>  {

}
