package fr.stephane.cinema.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import fr.stephane.cinema.models.Film;

public interface FilmRepository extends MongoRepository<Film, String> {

}
