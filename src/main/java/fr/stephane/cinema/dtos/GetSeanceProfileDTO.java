package fr.stephane.cinema.dtos;


import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GetSeanceProfileDTO {

	@Id
	private String id;
	private String nomCine;
	private String nomFilm;
	private LocalDateTime date;
	private LocalDateTime dateFin;
	private int numeroSalle;


	

	
}
