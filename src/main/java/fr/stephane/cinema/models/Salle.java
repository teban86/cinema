package fr.stephane.cinema.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Document
public class Salle {
	
	@Id
	private String id;
	private int numeroSalle;
	private int nbrPlace;
	@DBRef
	private Cinema cinema;

}
