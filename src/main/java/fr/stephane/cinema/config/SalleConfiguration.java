package fr.stephane.cinema.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.stephane.cinema.repositories.SalleRepository;
import fr.stephane.cinema.services.SalleService;

@Configuration
public class SalleConfiguration {
	@Bean
	public SalleService salleService(SalleRepository repo) {
		return new SalleService(repo);
	}
}
