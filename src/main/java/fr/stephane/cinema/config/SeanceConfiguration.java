package fr.stephane.cinema.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.stephane.cinema.repositories.SeanceRepository;
import fr.stephane.cinema.services.SeanceService;

@Configuration
public class SeanceConfiguration {

	@Bean
	public SeanceService seanceService(SeanceRepository repo, ModelMapper mapper) {
		return new SeanceService(repo, mapper);
	}
}
