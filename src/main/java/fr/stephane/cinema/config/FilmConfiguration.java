package fr.stephane.cinema.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.stephane.cinema.repositories.FilmRepository;
import fr.stephane.cinema.services.FilmService;

@Configuration
public class FilmConfiguration {

	@Bean
	public FilmService filmService(FilmRepository repo) {
		return new FilmService(repo);
	}
}
