package fr.stephane.cinema.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.stephane.cinema.repositories.CinemaRepository;
import fr.stephane.cinema.services.CinemaService;

@Configuration
public class CinemaConfiguration {
	
	@Bean
	public CinemaService cinemaService(CinemaRepository repo) {
		return new CinemaService(repo);
	}

	
}
