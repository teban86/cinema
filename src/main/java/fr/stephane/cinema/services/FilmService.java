package fr.stephane.cinema.services;

import java.util.List;
import java.util.Optional;

import fr.stephane.cinema.models.Film;
import fr.stephane.cinema.repositories.FilmRepository;

public class FilmService {

	private FilmRepository repo;
	
	public FilmService(FilmRepository repo) {
		this.repo = repo;
	}
	
	public List<Film> findAll(){
		return this.repo.findAll();
	}
	
	public Optional<Film> findbyId(String id){
		return this.repo.findById(id);
	}
	
	public Film save(Film film) {
		return this.repo.save(film);
	}
	
	public void delete(Film film) {
		this.repo.delete(film);
	}
	
	
	
}
