package fr.stephane.cinema.services;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;

import fr.stephane.cinema.dtos.GetSeanceProfileDTO;
import fr.stephane.cinema.exceptions.NotFoundException;
import fr.stephane.cinema.models.Seance;
import fr.stephane.cinema.repositories.SeanceRepository;

public class SeanceService {

	private SeanceRepository repo;

	private ModelMapper mapper;
	
	public SeanceService(SeanceRepository repo, ModelMapper mapper) {
		this.repo = repo;
		this.mapper = mapper;
		
	}
	
	public List<Seance> findAll(){
		return this.repo.findAll();
	}
	
	public Optional<Seance> findbyId(String id){
		return this.repo.findById(id);
	}
	
	public Seance save(Seance seance) {
		return this.repo.save(seance);
	}
	
	public void delete(Seance seance) {
		this.repo.delete(seance);
	}
	
	public Optional<GetSeanceProfileDTO> getProfil(String id) throws NotFoundException {
		Optional<Seance> seance = this.repo.findById(id);
		//GetSeanceProfileDTO getSeanceProfileDTO = null;
		Optional<GetSeanceProfileDTO> getSeanceProfileDTO;
		if (seance.isPresent()) {
			getSeanceProfileDTO = Optional.of(mapper.map(seance.get(), GetSeanceProfileDTO.class));
			getSeanceProfileDTO.get().setNomCine(seance.get().getSalle().getCinema().getNomCine());
			getSeanceProfileDTO.get().setDateFin(seance.get().getDate().plusHours( (long) seance.get().getFilm().getDuree()));
		} else {
			throw new NotFoundException("il n'y a pas de ticket");
		}
		
		//mapping
		return getSeanceProfileDTO;
	}
}
