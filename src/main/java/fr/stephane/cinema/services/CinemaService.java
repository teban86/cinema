package fr.stephane.cinema.services;

import java.util.List;
import java.util.Optional;

import fr.stephane.cinema.models.Cinema;
import fr.stephane.cinema.repositories.CinemaRepository;

public class CinemaService {

	private CinemaRepository repo;
	
	public CinemaService(CinemaRepository repo) {
		this.repo = repo;
	}
	
	public List<Cinema> findAll(){
		return this.repo.findAll();
	}
	
	public Optional<Cinema> findbyId(String id){
		return this.repo.findById(id);
	}
	
	public Cinema save(Cinema cinema) {
		return this.repo.save(cinema);
	}
	
	public void delete(Cinema cinema) {
		this.repo.delete(cinema);
	}
}
