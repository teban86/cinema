package fr.stephane.cinema.services;

import java.util.List;
import java.util.Optional;

import fr.stephane.cinema.models.Salle;
import fr.stephane.cinema.repositories.SalleRepository;

public class SalleService {

	private SalleRepository repo;
	
	public SalleService(SalleRepository repo) {
		this.repo = repo;
	}
	
	public List<Salle> findAll(){
		return this.repo.findAll();
	}
	
	public Optional<Salle> findbyId(String id){
		return this.repo.findById(id);
	}
	
	public Salle save(Salle salle) {
		return this.repo.save(salle);
	}
	
	public void delete(Salle salle) {
		this.repo.delete(salle);
	}
}
