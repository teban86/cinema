package fr.stephane.cinema.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.stephane.cinema.models.Salle;
import fr.stephane.cinema.services.SalleService;

@RestController
@RequestMapping("/salles")
public class SalleController {
	
	SalleService service;
	
	public SalleController(SalleService service) {
		this.service = service;
	}
	
	/**
	 * Méthode qui retourne tous les Salle
	 * @return List<Salle>
	 */
	@GetMapping
	public List<Salle> finAll(){
		return service.findAll();
	}
	
	/**
	 * Méthode qui retourne une Salle par rapport à son id
	 * @param id
	 * @return Salle
	 */
	@GetMapping("{id}")
	public Optional<Salle> findBySlug(@PathVariable String id){
		return this.service.findbyId(id);
	}
	
	/**
	 * Méthode qui permet de creer une Salle
	 * @param Salle
	 * @return Salle
	 */
	@PostMapping
	public Salle save(@RequestBody Salle salle) {
		return this.service.save(salle);
	}
	
	/**
	 * Méthode qui permet de mettre à jour une Salle
	 * @param Salle
	 * @return Salle
	 */
	@PutMapping
	public Salle modif(@RequestBody Salle salle) {
		return this.service.save(salle);
	}
	
	/**
	 * Méthode qui permet de supprimer une salle
	 * @param Salle
	 */
	@DeleteMapping
	public void delete(@RequestBody Salle salle) {
		this.service.delete(salle);
	}

}
