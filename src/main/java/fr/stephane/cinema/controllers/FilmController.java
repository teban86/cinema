package fr.stephane.cinema.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import fr.stephane.cinema.models.Film;
import fr.stephane.cinema.services.FilmService;

@RestController
@RequestMapping("/films")
public class FilmController {
	
	FilmService service;
	
	public FilmController(FilmService service) {
		this.service = service;
	}
	
	/**
	 * Méthode qui retourne tous les films
	 * @return List<Film>
	 */
	@GetMapping
	public List<Film> finAll(){
		return service.findAll();
	}
	
	/**
	 * Méthode qui retourne un film par rapport à son id
	 * @param id
	 * @return Film
	 */
	@GetMapping("{id}")
	public Optional<Film> findBySlug(@PathVariable String id){
		return this.service.findbyId(id);
	}
	
	/**
	 * Méthode qui permet de creer un film
	 * @param film
	 * @return Film
	 */
	@PostMapping
	public Film save(@RequestBody Film film) {
		return this.service.save(film);
	}
	
	/**
	 * Méthode qui permet de mettre à jour un film
	 * @param film
	 * @return Film
	 */
	@PutMapping
	public Film modif(@RequestBody Film film) {
		return this.service.save(film);
	}
	
	/**
	 * Méthode qui permet de supprimer un film
	 * @param film
	 */
	@DeleteMapping
	public void delete(@RequestBody Film film) {
		this.service.delete(film);
	}

}
