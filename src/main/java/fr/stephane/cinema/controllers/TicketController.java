package fr.stephane.cinema.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.stephane.cinema.dtos.GetSeanceProfileDTO;
import fr.stephane.cinema.exceptions.NotFoundException;
import fr.stephane.cinema.models.Seance;
import fr.stephane.cinema.services.SeanceService;

@RestController
@RequestMapping("/tickets")
public class TicketController {

	SeanceService service;
	
	public TicketController(SeanceService service) {
		this.service =service;
	}
	
	@GetMapping
	public List<Seance> finAll(){
		return service.findAll();
	}
	
	@GetMapping("myticket/{commande}")
	public ResponseEntity<GetSeanceProfileDTO> getSeanceProfil(@PathVariable String commande) {
		try {
			Optional<GetSeanceProfileDTO> getSeanceProfileDTO = this.service.getProfil(commande);
			return ResponseEntity.of(getSeanceProfileDTO);	
		} catch (NotFoundException e) {
			return ResponseEntity.notFound().header(commande, "il n'y a pas de ticket").build();
		}
	}
	
}
