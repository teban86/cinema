package fr.stephane.cinema.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.stephane.cinema.models.Cinema;
import fr.stephane.cinema.services.CinemaService;


@RestController
@RequestMapping("/cinemas")
public class CinemaController {

	CinemaService service;
	
	public CinemaController(CinemaService service) {
		this.service = service;
	}
	
	/**
	 * Méthode qui retourne tous les cinemas
	 * @return List<cinema>
	 */
	@GetMapping
	public List<Cinema> finAll(){
		return service.findAll();
	}
	
	/**
	 * Méthode qui retourne un cinema par rapport à son id
	 * @param id
	 * @return cinema
	 */
	@GetMapping("{id}")
	public Optional<Cinema> findBySlug(@PathVariable String id){
		return this.service.findbyId(id);
	}
	
	/**
	 * Méthode qui permet de creer un cinema
	 * @param cinema
	 * @return cinema
	 */
	@PostMapping
	public Cinema save(@RequestBody Cinema cinema) {
		return this.service.save(cinema);
	}
	
	/**
	 * Méthode qui permet de mettre à jour un cinema
	 * @param cinema
	 * @return cinema
	 */
	@PutMapping
	public Cinema modif(@RequestBody Cinema cinema) {
		return this.service.save(cinema);
	}
	
	/**
	 * Méthode qui permet de supprimer un cinema
	 * @param cinema
	 */
	@DeleteMapping
	public void delete(@RequestBody Cinema cinema) {
		this.service.delete(cinema);
	}
}
