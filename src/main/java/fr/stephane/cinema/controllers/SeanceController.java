package fr.stephane.cinema.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.stephane.cinema.models.Seance;
import fr.stephane.cinema.services.SeanceService;

@RestController
@RequestMapping("/seances")
public class SeanceController {

	SeanceService service;
	
	public SeanceController(SeanceService service) {
		this.service = service;
	}
	
	/**
	 * Méthode qui retourne toutes les Seances
	 * @return List<Seance>
	 */
	@GetMapping
	public List<Seance> finAll(){
		return service.findAll();
	}
	
	/**
	 * Méthode qui retourne une Seance par rapport à son id
	 * @param id
	 * @return Seance
	 */
	@GetMapping("{id}")
	public Optional<Seance> findBySlug(@PathVariable String id){
		return this.service.findbyId(id);
	}
	
	/**
	 * Méthode qui permet de creer une Seance
	 * @param Seance
	 * @return Seance
	 */
	@PostMapping
	public Seance save(@RequestBody Seance seance) {
		return this.service.save(seance);
	}
	
	/**
	 * Méthode qui permet de mettre à jour une Seance
	 * @param Seance
	 * @return Seance
	 */
	@PutMapping
	public Seance modif(@RequestBody Seance seance) {
		return this.service.save(seance);
	}
	
	/**
	 * Méthode qui permet de supprimer une Seance
	 * @param Seance
	 */
	@DeleteMapping
	public void delete(@RequestBody Seance seance) {
		this.service.delete(seance);
	}
	
}
